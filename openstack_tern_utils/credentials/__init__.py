import os

AUTH_PARAMS = ['auth_url', 'username', 'password', 'project_id', 'user_domain_name', 'project_domain_name']


def get_nova_credentials_v2(auth: dict = None):
    if auth is not None:
        if all(param in AUTH_PARAMS for param in auth):
            auth.update({'version': '2'})
            return auth
        else:
            print("ERROR: Authentication field missed in dictionary. Mandatory fields are {}".format(AUTH_PARAMS))
            exit(1)
    else:
        return {
            'version': '2',
            'auth_url': os.environ['OS_AUTH_URL'],
            'username': os.environ['OS_USERNAME'],
            'password': os.environ['OS_PASSWORD'],
            'project_id': os.environ['OS_PROJECT_ID'],
            'user_domain_name': os.environ['OS_USER_DOMAIN_NAME'],
            'project_domain_name': os.environ['OS_PROJECT_DOMAIN_ID']
        }


def get_swift_credentials(auth: dict = None):
    if auth is not None:
        if all(param in AUTH_PARAMS for param in auth):
            return {
                'auth_version': '3',
                'authurl': auth['auth_url'],
                'user': auth['username'],
                'key': auth['password'],
                'os_options': {
                    'project_id': auth['project_id'],
                    'user_domain_name': auth['user_domain_name'],
                    'project_domain_name': auth['project_domain_name']
                }
            }
        else:
            print("ERROR: Authentication field missed in dictionary. Mandatory fields are {}".format(AUTH_PARAMS))
            exit(1)
    else:
        return {
            'auth_version': '3',
            'authurl': os.environ['OS_AUTH_URL'],
            'user': os.environ['OS_USERNAME'],
            'key': os.environ['OS_PASSWORD'],
            'os_options': {
                'project_id': os.environ['OS_PROJECT_ID'],
                'user_domain_name': os.environ['OS_USER_DOMAIN_NAME'],
                'project_domain_name': os.environ['OS_PROJECT_DOMAIN_ID']
            }
        }
