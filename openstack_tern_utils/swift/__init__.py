import swiftclient

from openstack_tern_utils.credentials import get_swift_credentials


def list_containers(auth=None):
    conn = swiftclient.Connection(**get_swift_credentials(auth))
    resp_headers, containers = conn.get_account()
    print("Response headers: %s" % resp_headers)
    return containers


def create_container(container, auth=None):
    conn = swiftclient.Connection(**get_swift_credentials(auth))
    conn.put_container(container)
    resp_headers, containers = conn.get_account()
    if container in containers:
        print("The container was created")


def put_object(container, object, dest_object, content_type, auth=None):
    conn = swiftclient.Connection(**get_swift_credentials(auth))
    with open(object, 'rb') as obj:
        conn.put_object(container, dest_object,
                        contents=obj,
                        content_type=content_type)
    try:
        conn.head_object(container, dest_object)
        print('The object was successfully created')
    except swiftclient.ClientException as e:
        if str(e.http_status) == '404':
            print('The object was not found')
        else:
            print('An error occurred checking for the existence of the object')
        exit(1)


def get_object(container, object, dest_object, auth=None):
    conn = swiftclient.Connection(**get_swift_credentials(auth))
    try:
        resp_headers, obj_contents = conn.get_object(container, object)
        with open(dest_object, 'wb') as obj:
            obj.write(obj_contents)
    except swiftclient.ClientException as e:
        if str(e.http_status) == '404':
            print('The object was not found')
        else:
            print('An error occurred checking for the existence of the object')
        exit(1)


def delete_object(container, object, auth=None):
    conn = swiftclient.Connection(**get_swift_credentials(auth))
    try:
        conn.delete_object(container, object)
        print("Successfully deleted the object")
    except swiftclient.ClientException as e:
        print("Failed to delete the object with error: %s" % e)
        exit(1)

# if __name__ == "__main__":
#     print(list_containers())
#     put_object("ecoplatform-postgres-backup", "local.txt", "destloccaal.txt", "text/plain")
#     get_object("ecoplatform-postgres-backup", "destloccaal.txt", "desloc.txt")
#     delete_object("ecoplatform-postgres-backup", "destloccaal.txt")
