import argparse

from openstack_tern_utils.instances import create_instance, delete_instance
from openstack_tern_utils.swift import create_container, list_containers, put_object, get_object, delete_object


def main():
    parser = argparse.ArgumentParser(description='Manage Nectar OpenStack cloud through CLI.')
    subparsers = parser.add_subparsers(help='Performing action')

    # Launch instance
    create_server_parser = subparsers.add_parser('create-server', help='Create a dedicated VM in Nectar')
    create_server_parser.add_argument('--action', action="store", default="create-server", help=argparse.SUPPRESS)
    create_server_parser.add_argument('-n', '--name', action="store", required=True)
    create_server_parser.add_argument('-i', '--image', action="store", required=False)
    create_server_parser.add_argument('-f', '--flavor', action="store", required=False)
    create_server_parser.add_argument('-k', '--key', action="store", required=False)
    create_server_parser.add_argument('-nw', '--network', nargs='*', action="store", required=False)
    create_server_parser.add_argument('-s', '--security-groups', dest="security_groups", action="store", nargs='*',
                                      required=False)
    create_server_parser.add_argument('-a', '--availability-zone', dest="availability_zone", action="store",
                                      required=False)
    create_server_parser.add_argument('-u', '--user-data', dest="user_data", action="store", required=False)

    # Delete instance
    delete_server_parser = subparsers.add_parser('delete-server', help='Delete a specified VM in Nectar')
    delete_server_parser.add_argument('--action', action="store", default="delete-server", help=argparse.SUPPRESS)
    delete_server_parser.add_argument('--id', action="store", required=True)

    # List containers (swift)
    list_containers_parser = subparsers.add_parser('list-containers', help='List containers in Nectar Swift Storage')
    list_containers_parser.add_argument('--action', action="store", default="list-containers", help=argparse.SUPPRESS)

    # Create container (swift)
    create_container_parser = subparsers.add_parser('create-container',
                                                    help='Create a container in Nectar Swift Storage')
    create_container_parser.add_argument('--action', action="store", default="create-container", help=argparse.SUPPRESS)
    create_container_parser.add_argument('--container-name', dest="container_name", action="store", required=True)

    # Put object in container (swift)
    put_object_parser = subparsers.add_parser('put-object', help='Put object in container in Nectar Swift Storage')
    put_object_parser.add_argument('--action', action="store", default="put-object", help=argparse.SUPPRESS)
    put_object_parser.add_argument('--container-name', dest="container_name", action="store", required=True)
    put_object_parser.add_argument('--object', dest="object", action="store", required=True)
    put_object_parser.add_argument('--dest-object', dest="dest_object", action="store", required=True)
    put_object_parser.add_argument('--content-type', dest="content_type", action="store", required=True)

    # Get object from container (swift)
    get_object_parser = subparsers.add_parser('get-object', help='Get object from container in Nectar Swift Storage')
    get_object_parser.add_argument('--action', action="store", default="get-object", help=argparse.SUPPRESS)
    get_object_parser.add_argument('--container-name', dest="container_name", action="store", required=True)
    get_object_parser.add_argument('--object', dest="object", action="store", required=True)
    get_object_parser.add_argument('--dest-object', dest="dest_object", action="store", required=True)

    # Delete object from container (swift)
    delete_object_parser = subparsers.add_parser('delete-object',
                                                 help='Delete object from container in Nectar Swift Storage')
    delete_object_parser.add_argument('--action', action="store", default="delete-object", help=argparse.SUPPRESS)
    delete_object_parser.add_argument('--container-name', dest="container_name", action="store", required=True)
    delete_object_parser.add_argument('--object', dest="object", action="store", required=True)

    args = parser.parse_args()
    # print(args)

    if args.action == "create-server":
        instance = create_instance(name=args.name,
                                   image=args.image,
                                   nets=args.network,
                                   security_groups=args.security_groups,
                                   availability_zone=args.availability_zone,
                                   user_data=args.user_data)
        print("Instance {}:{} created and running with success".format(instance.name, instance.id))
    elif args.action == "delete-server":
        instance = delete_instance(id=args.id)
        print("Instance {} deleted with success".format(instance))
    elif args.action == "list-containers":
        containers = list_containers()
        print("Containers: {} created with success".format(containers))
    elif args.action == "create-container":
        create_container(container=args.container_name)
        print("Container '{}' created with success".format(args.container_name))
    elif args.action == "put-object":
        put_object(container=args.container_name, object=args.object, dest_object=args.dest_object,
                   content_type=args.content_type)
        print("Object '{}' uploaded with success".format(args.object))
    elif args.action == "get-object":
        get_object(container=args.container_name, object=args.object, dest_object=args.dest_object)
        print("Object '{}' downloaded with success".format(args.object))
    elif args.action == "delete-object":
        delete_object(container=args.container_name, object=args.object)
        print("Object '{}' deleted with success".format(args.object))
    else:
        print("ERROR: Requested action is not implemented.")
        exit(1)


if __name__ == "__main__":
    main()
