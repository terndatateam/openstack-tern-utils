import time

from novaclient.client import Client
from novaclient.exceptions import NotFound

from openstack_tern_utils.credentials import get_nova_credentials_v2

NECTAR_DEFAULT_IMAGE = "NeCTAR CentOS 8 x86_64"

FLAVOR_XXLARGE = "m3.xxlarge"
FLAVOR_XLARGE = "m3.xlarge"
FLAVOR_LARGE = "m3.large"
FLAVOR_MEDIUM = "m3.medium"
FLAVOR_RO_XXLARGE = "r3.xxlarge"
FLAVOR_RO_XLARGE = "r3.xlarge"
FLAVOR_RO_LARGE = "r3.large"
FLAVOR_RO_MEDIUM = "r3.medium"


def create_instance(name, image=NECTAR_DEFAULT_IMAGE, flavor=None, ram_optimized_flavor=False, key="ansible", nets=[],
                    security_groups=["default"], availability_zone="QRIScloud", user_data=None, auth=None):
    nova_client = Client(**get_nova_credentials_v2(auth))

    if flavor is None:
        limits: dict = nova_client.limits.get()
        quota_usage = limits._info["absolute"]
        print(quota_usage)
        if quota_usage["maxTotalCores"] - quota_usage["totalCoresUsed"] >= 32 and quota_usage["maxTotalRAMSize"] - \
                quota_usage["totalRAMUsed"] >= 64:
            if ram_optimized_flavor and quota_usage["maxTotalRAMSize"] - quota_usage["totalRAMUsed"] >= 128:
                suitable_flavor = FLAVOR_RO_XXLARGE
            else:
                suitable_flavor = FLAVOR_XXLARGE
        elif quota_usage["maxTotalCores"] - quota_usage["totalCoresUsed"] >= 16 and quota_usage["maxTotalRAMSize"] - \
                quota_usage["totalRAMUsed"] >= 32:
            if ram_optimized_flavor and quota_usage["maxTotalRAMSize"] - quota_usage["totalRAMUsed"] >= 64:
                suitable_flavor = FLAVOR_RO_XLARGE
            else:
                suitable_flavor = FLAVOR_XLARGE
        elif quota_usage["maxTotalCores"] - quota_usage["totalCoresUsed"] >= 8 and quota_usage["maxTotalRAMSize"] - \
                quota_usage["totalRAMUsed"] >= 16:
            if ram_optimized_flavor and quota_usage["maxTotalRAMSize"] - quota_usage["totalRAMUsed"] >= 32:
                suitable_flavor = FLAVOR_RO_LARGE
            else:
                suitable_flavor = FLAVOR_LARGE
        elif quota_usage["maxTotalCores"] - quota_usage["totalCoresUsed"] >= 4 and quota_usage["maxTotalRAMSize"] - \
                quota_usage["totalRAMUsed"] >= 8:
            if ram_optimized_flavor and quota_usage["maxTotalRAMSize"] - quota_usage["totalRAMUsed"] >= 16:
                suitable_flavor = FLAVOR_RO_MEDIUM
            else:
                suitable_flavor = FLAVOR_MEDIUM
        else:
            print(
                "ERROR: The current quota usage does not allow to boot a VM with at least 4 cores and 8 GB of RAM. "
                "Please review your quota.")
            exit(1)
    else:
        suitable_flavor = flavor

    # server = nova_client.servers.get("f636b17e-d268-46d7-bb39-fae8163bfa72")
    # print(nova_client.keypairs.list())

    key = nova_client.keypairs.get(key)
    image = nova_client.glance.find_image(image)
    flavor = nova_client.flavors.find(name=suitable_flavor)
    default_net = nova_client.neutron.find_network("Classic Provider")
    nics = [{'net-id': default_net.id}]
    for net_name in nets:
        net = nova_client.neutron.find_network(net_name)
        nics.append({'net-id': net.id})

    # print(image)
    # print(flavor)
    # print(net)

    # print(nova_client.servers.list())

    instance = nova_client.servers.create(name=name,
                                          image=image,
                                          flavor=flavor,
                                          security_groups=security_groups,
                                          availability_zone=availability_zone,
                                          key_name=key.id,
                                          nics=nics,
                                          userdata=user_data
                                          )

    print("Sleeping for 5s after create command")
    time.sleep(5)

    check_instance = nova_client.servers.get(instance)
    while check_instance.status != 'ACTIVE' and check_instance.status != 'ERROR':
        print("Building instance... please wait...")
        time.sleep(30)
        check_instance = nova_client.servers.get(instance)

    if check_instance.status == 'ERROR':
        print("Error building instance. Please check OpenStack logs.")
        print(check_instance.fault)
        exit(1)

    print("VM running... status=" + nova_client.servers.get(instance).status)
    return instance

    # print(nova_client.servers.list())


def delete_instance(id=id, auth=None):
    nova_client = Client(**get_nova_credentials_v2(auth))

    try:
        instance = nova_client.servers.get(id)
    except NotFound as e:
        print("ERROR: Instance ID specified does not exists. Please review the ID provided.")
        print(e)
        exit(1)

    nova_client.servers.delete(instance)

    try:
        check_instance = nova_client.servers.get(id)
        while check_instance is not None and check_instance.status != "ERROR":
            print("Deleting instance... please wait...")
            time.sleep(20)
            check_instance = nova_client.servers.get(id)
    except NotFound:
        return id

    if check_instance.status == "ERROR":
        print("ERROR: Instance could not be deleted. Please check Nectar logs for more information.")
        print(check_instance.fault)
        exit(1)
