import setuptools

with open("Readme.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as req:
    install_requires = req.read().split("\n")

setuptools.setup(
    name="openstack_tern_utils",
    version="0.1.4",
    author="Javier Sanchez Gonzalez",
    author_email="j.sanchezgonzalez@uq.edu.au",
    description="A Python library with multiple methods to manage Nectar OpenStack cloud.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/terndatateam/openstack-tern-utils",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=install_requires,
)
