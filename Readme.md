# Nectar Openstack Utils for TERN

This Python library include multiple methods to help managing the Nectar OpenStack cloud using the 'python-openstackclient' (Python OpenStack JDK).

This current version (0.1.3) supports:

- Create / delete instances programmatically.
- List / create containers programmatically.
- Put / get / delete objects programmatically.

### Authentication

To authenticate the novaclient (openstack client) against the Nectar cloud it requires the following variables available in the Enviroment:

- OS_AUTH_URL
- OS_USERNAME
- OS_PASSWORD
- OS_PROJECT_ID
- OS_USER_DOMAIN_NAME
- OS_PROJECT_DOMAIN_ID

All this values can be easily found in the OpenStack authentication script file. This file can be downloaded from your Nectar account:

1. Login to the NeCTAR Cloud Dashboard
2. Select a project name from the project drop down list
3. Click 'Access & Security'
4. On the 'Access & Security' page, click tab 'API Access'
5. Click the button: "Download OpenStack RC File"
6. Save the file into a directory
7. Click the drop down list with your email on the right top of page, then click Settings.
8. Click the 'Reset Password' button and save the password that appears on the screen. This is the API password.

**However, it is highly recommended to use the most generic account (e.g. tern.data@uq.edu.au) instead of your personal one.** 

Additionally, authentication information can be pass in programmatically through a well defined dictionary:

```buildoutcfg
auth_dict = {   'auth_url': 'https://keystone.rc.nectar.org.au:5000/v3/',
                'username': 'test@uq.edu.au',
                'password': 'password',
                'project_id': 'project_id',
                'user_domain_name': 'default',
                'project_domain_name': 'default'
            }    

instance = delete_instance(id=id, auth=auth_dict)
```

### Usage

This library can be used through CLI commands or programmatically:

#### CLI
```buildoutcfg
>>> os_tern.py --help

usage: os_tern.py [-h]
                  {create-server,delete-server,list-containers,create-container,put-object,get-object,delete-object}
                  ...

Manage Nectar OpenStack cloud through CLI.

positional arguments:
  {create-server,delete-server,list-containers,create-container,put-object,get-object,delete-object}
                        Performing action
    create-server       Create a dedicated VM in Nectar
    delete-server       Delete a specified VM in Nectar
    list-containers     List containers in Nectar Swift Storage
    create-container    Create a container in Nectar Swift Storage
    put-object          Put object in container in Nectar Swift Storage
    get-object          Get object from container in Nectar Swift Storage
    delete-object       Delete object from container in Nectar Swift Storage

optional arguments:
  -h, --help            show this help message and exit
```

#### Programmatically

```buildoutcfg
# Create instance
instance = create_instance(name=name,
                           image=image,
                           nets=network,
                           security_groups=security_groups)

# Delete instance by ID
instance = delete_instance(id=id)
```

### Improvements

### Contact
**Javier Sanchez**  
*Software Engineer*  
[j.sanchezgonzalez@uq.edu.au](mailto:j.sanchezgonzalez@uq.edu.au)  